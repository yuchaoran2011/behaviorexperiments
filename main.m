clear; clc;

Screen('Preference', 'SkipSyncTests', 1);
% Part One: Initialization
Screen('Preference', 'VisualDebugLevel', 1);
window = Screen('OpenWindow', 0);
Screen('TextSize', window, 24);
Screen('TextFont', window, 'Times');

% Parameter settings
deltaValue = .1; % Delta value used in delta inference
numOfPairs = 2; % Number of questions/pages
numOfProperties = 5; % Number of queues for an object
numOfTrialQuestions = 2; % Number of training questions, must be <= numOfPairs
pairs = createPairs(numOfPairs, numOfProperties, deltaValue);
% If ttb strategy is used, line above should be changed to the following
% pairs = createPairs(numOfPairs, numOfProperties);

Cursorsize = 6;  % How big our mouse cursor will be
SetMouse(50, 50, window); % Move the mouse to a specific spot
 
% Part Two: Mousewait
buttons = 1;
while any(buttons)
    [Mousex, Mousey, buttons] = GetMouse(window); 
end
 
% Part Three: Collect Data
DesiredSampleRate = 1000;  % Number of samples per second
begintime = GetSecs;
nextsampletime = begintime;

coord1 = [500 200];
coord2 = [800 200];

isWithinBox = 0;
lastStatus = 0;
lastBoxNum = intmax('int64');

ins = Instructions();
ins.readInstructionsFromFile('instructions.txt');

for k=0:numOfPairs
    stayDuration = 0;
    flipBox = 0;
    sample = 0;
    while 1
        if k == 0
            ins.drawText(window);
            Screen('Flip', window);
            KbStrokeWait;
            break;
        end 
        
        [Mousex, Mousey, buttons] = GetMouse(window);
        [clicked, choice] = pairs(k).choiceMade(Mousex, Mousey);
        if buttons(1) == 1 && clicked 
            if k <= numOfTrialQuestions  
                if pairs(k).checkAnswer(choice)
                    Screen('DrawText', window, 'Congratulations! Your answer is correct! Hit any key to continue to the next question.', 350, 450, [0, 255, 0]);
                else
                    Screen('DrawText', window, 'Sorry! You have made the wrong choice! Hit any key to continue to the next question.', 350, 450, [128, 0, 128]);
                end
                Screen('Flip', window); 
                KbStrokeWait;
            end
            break;
        end
        
        sample = sample + 1;
       
        pairs(k).MouseData(sample, 1) = Mousex;
        pairs(k).MouseData(sample, 2) = Mousey;

        [currObjNum, currCueNum] = mouseIsWithinBox(Mousex, Mousey, pairs(k).BoxPos, numOfProperties);
        
        if (currObjNum == intmax('int64'))
            currBoxNum = currObjNum;
        else
            currBoxNum = (currObjNum-1) * numOfProperties + currCueNum;                  
        end

        if (currBoxNum ~= intmax('int64'))
            increment = 1 / DesiredSampleRate;
            
            if (currBoxNum == lastBoxNum)
                stayDuration = stayDuration + increment;
                if (stayDuration >= 100 / DesiredSampleRate)
                    flipBox = 1;
                    stayDuration = 0;
                end
            else
                stayDuration = 0;
                flipBox = 0;
            end        
            if (flipBox)
                if (currObjNum == 1)
                    pairs(k).Object1.updateDuration(currCueNum, increment);
                else
                    pairs(k).Object2.updateDuration(currCueNum, increment);
                end
            end
        end
        
        if ((currBoxNum ~= lastBoxNum && currBoxNum ~= intmax('int64')) || currBoxNum == intmax('int64'))        
            
            %{
            fileName = strcat(num2str(currBoxNum), num2str(k), '.jpg');
            fullFileName = strcat('images/', fileName);
            if ~exist(fullFileName, 'file')
                if ~exist('images', 'dir')
                    mkdir('images');
                end
                imageArray = Screen('GetImage', window);
                imwrite(imageArray, fullFileName);
            end
            %}
            
            pairs(k).Object1.drawObject(window, [500 100]);
            pairs(k).Object2.drawObject(window, [800 100]);     
            pairs(k).drawChoiceBoxes(window);
            pairs(k).drawCueNames(window);
            Screen('DrawText', window, strcat(num2str(k), '/', num2str(numOfPairs)), 100, 100, [0,0,0]);
            Screen('Flip', window);
        end

        if (flipBox && currBoxNum ~= intmax('int64'))  
            pairs(k).Object1.drawTitle(window, [500 100]);
            pairs(k).Object2.drawTitle(window, [800 100]);

            increment = 100 * (currCueNum - 1);
            if (currObjNum == 1)
                pairs(k).Object1.drawUponSelect(window, currCueNum, coord1(1), coord1(2) + increment);
                pairs(k).Object2.drawBoxes(window);
                pairs(k).Object1.updateNumClicks(currCueNum);
            else
                pairs(k).Object2.drawUponSelect(window, currCueNum, coord2(1), coord2(2) + increment);
                pairs(k).Object1.drawBoxes(window);
                pairs(k).Object2.updateNumClicks(currCueNum)
            end

            % Draw choice boxes, cue names and paging
            pairs(k).drawChoiceBoxes(window);
            pairs(k).drawCueNames(window);
            Screen('DrawText', window, strcat(num2str(k), '/', num2str(numOfPairs)), 100, 100, [0,0,0]);     
             
            Screen('flip', window);
            
            % Screenshot capture       
            fileName = strcat(num2str(k), '_', num2str(currBoxNum), '.jpg');
            fullFileName = strcat('images/', fileName);
            if ~exist(fullFileName, 'file')
                mkdir('images');
                imageArray = Screen('GetImage', window);
                imwrite(imageArray, fullFileName);
            end
        end
         
        lastBoxNum = currBoxNum;

        sampletime(sample) = GetSecs;
        nextsampletime = nextsampletime + 1/DesiredSampleRate;
        while GetSecs < nextsampletime
        end
    end
    
    % Get ready to move to next question/pair, if their are any remaning to
    % be answered.
    if k ~= 0
        buttons = 1;
        while any(buttons)
            [Mousex,Mousey,buttons] = GetMouse(window); 
        end
    end        
end
 
% Part Four: Cleanup
endtime = GetSecs;
ElapsedTime = endtime - begintime;
NumberOfSamples = sample;
ActualSampleRate = 1/(ElapsedTime / NumberOfSamples);
% mousedata = mousedata(1:sample,1:2);
ShowCursor;
sca;

% Replays mouse data for first pair
replay(pairs(1));

% Mouse trace plot for question 1
%{
figure;
set(gca,'xaxislocation','top','yaxislocation','left','ydir','reverse');
a = pairs(2);
data_a = a.MouseData;
plot(data_a(:,1), data_a(:,2));
%}

%{ 
clf;
plot(mousedata(:,1), mousedata(:,2));
set(gca,'YDir','reverse');
axis equal;
%}

% Animated mouse trace
%{
trace = animatedline;
for k = 1:size(mousedata, 1)
    addpoints(trace, mousedata(k, 1), mousedata(k, 2));
    drawnow
end
%}
