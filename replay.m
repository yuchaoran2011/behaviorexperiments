function replay(pair)
    Screen('Preference', 'SkipSyncTests', 1);
    Screen('Preference', 'VisualDebugLevel', 1);
    window = Screen('OpenWindow', 0);
    Screen('TextSize', window, 24);
    Screen('TextFont', window, 'Times');

    numOfProperties = 5; % Number of queues for an object
    DesiredSampleRate = 100;  % Mouse replay rate
    nextsampletime = GetSecs;
    waitTime = 3; % Number of seconds to wait before closing screen

    coord1 = [500 200];
    coord2 = [800 200];
    mouseData = pair.MouseData;

    lastBoxNum = intmax('int64');
    stayDuration = 0;
    flipBox = 0;
    
    for i=1:size(mouseData, 1)
        Mousex = mouseData(i, 1);
        Mousey = mouseData(i, 2);
        
        SetMouse(Mousex, Mousey, window);

        [currObjNum, currCueNum] = mouseIsWithinBox(Mousex, Mousey, pair.BoxPos, numOfProperties);
        
        if (currObjNum == intmax('int64'))
            currBoxNum = currObjNum;
        else
            currBoxNum = (currObjNum-1) * numOfProperties + currCueNum;                  
        end

        if (currBoxNum ~= intmax('int64'))
            increment = 1 / DesiredSampleRate;
            
            if (currBoxNum == lastBoxNum)
                stayDuration = stayDuration + increment;
                if (stayDuration >= 100 / DesiredSampleRate)
                    flipBox = 1;
                    stayDuration = 0;
                end
            else
                stayDuration = 0;
                flipBox = 0;
            end        
        end
        
        if ((currBoxNum ~= lastBoxNum && currBoxNum ~= intmax('int64')) || currBoxNum == intmax('int64'))
            pair.Object1.drawObject(window, [500 100]);
            pair.Object2.drawObject(window, [800 100]);     
            pair.drawChoiceBoxes(window);
            pair.drawCueNames(window);
            Screen('Flip', window);
        end

        if (flipBox && currBoxNum ~= intmax('int64'))  
            pair.Object1.drawTitle(window, [500 100]);
            pair.Object2.drawTitle(window, [800 100]);

            increment = 100 * (currCueNum - 1);
            if (currObjNum == 1)
                pair.Object1.drawUponSelect(window, currCueNum, coord1(1), coord1(2) + increment);
                pair.Object2.drawBoxes(window);
            else
                pair.Object2.drawUponSelect(window, currCueNum, coord2(1), coord2(2) + increment);
                pair.Object1.drawBoxes(window);
            end

            % Draw choice boxes, cue names and paging
            pair.drawChoiceBoxes(window);
            pair.drawCueNames(window);             
            Screen('flip', window);
        end
         
        lastBoxNum = currBoxNum;

        nextsampletime = nextsampletime + 1/DesiredSampleRate;
        while GetSecs < nextsampletime
        end
    end
    
    currTime = GetSecs;
    while GetSecs < currTime + waitTime;
    end
    sca;
end
