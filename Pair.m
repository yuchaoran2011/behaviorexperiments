% A pair of objects, 
% equivalent to one question that asks to decide between the two objects.
classdef Pair < handle
   properties
      Object1
      Object2
      MouseData
      BoxPos
      AttrNames
      SelectedObject
      ChoiceBoxPos1
      ChoiceBoxPos2
      Choice
      CorrectAnswer
   end
   
   methods
       function obj = Pair()
           obj.ChoiceBoxPos1 = [450 750 600 800];
           obj.ChoiceBoxPos2 = [750 750 900 800];
       end
       
       function drawCueNames(object, window)
          cueNamePos = [250, 225];  
          names = object.AttrNames;
          for i=1:length(names)
              Screen('DrawText', window, names{i}, cueNamePos(1), cueNamePos(2)+100*(i-1), [0,0,0]);
          end
       end
       
       function drawChoiceBoxes(object, window)
           textPos1 = [500, 775];
           textPos2 = [800, 775];
           
           Screen('DrawText', window, 'Car A', textPos1(1), textPos1(2), [0,0,0]);
           Screen('FrameRect', window, [0, 0, 0], object.ChoiceBoxPos1);  
           Screen('DrawText', window, 'Car B', textPos2(1), textPos2(2), [0,0,0]);
           Screen('FrameRect', window, [0, 0, 0], object.ChoiceBoxPos2); 
       end
       
       function [clicked, choice] = choiceMade(object, mouseX, mouseY)
           clickedA = mouseX >= object.ChoiceBoxPos1(1) && mouseX <= object.ChoiceBoxPos1(3) && ...
                     mouseY >= object.ChoiceBoxPos1(2) && mouseY <= object.ChoiceBoxPos1(4);
           clickedB = mouseX >= object.ChoiceBoxPos2(1) && mouseX <= object.ChoiceBoxPos2(3) && ...
                     mouseY >= object.ChoiceBoxPos2(2) && mouseY <= object.ChoiceBoxPos2(4);
           clicked = clickedA || clickedB;
           if clickedA
               object.Choice = 'A';
           elseif clickedB
               object.Choice = 'B';
           else
               object.Choice = 'None';
           end
           choice = object.Choice;
       end
       
       function correct = checkAnswer(object, answer)
           correct = object.CorrectAnswer == answer;
       end
   end
end
