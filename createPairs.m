function pairs = createPairs(numOfPairs, numOfProperties, deltaValue)
    for n=numOfPairs:-1:1
        pairs(n) = Pair();
    end
    
    cueNames = cell(numOfProperties, 1);
    cueNames{1} = 'Mileage';
    cueNames{2} = 'Year';
    cueNames{3} = 'Price';
    cueNames{4} = 'Resale Price';
    cueNames{5} = 'Gas Efficiency';
    
    % Specify screen coordinates of boxes
    rect1 = zeros(4, numOfProperties);
    rect2 = zeros(4, numOfProperties);
    base1 = [450 200 600 250];
    base2 = [750 200 900 250];
    for i=1:numOfProperties
        rect1(:, i) = [base1(1) base1(2)+100*(i-1) base1(3) base1(4)+100*(i-1)];
        rect2(:, i) = [base2(1) base2(2)+100*(i-1) base2(3) base2(4)+100*(i-1)];
    end
    rect = [rect1 rect2];
    
    % Read cue values from Excel
    nums = xlsread('stimuli.xlsx');
    
    for i=1:numOfPairs
        object1 = Object(strcat('Car ', num2str(i), 'A'), numOfProperties);
        object2 = Object(strcat('Car ', num2str(i), 'B'), numOfProperties);
        cueVals1 = zeros(numOfProperties, 1);
        cueVals2 = zeros(numOfProperties, 1);
            
        for j=1:numOfProperties
            % Randomly assign cue values
            %cueVals1(j) = round(rand, 2);
            %cueVals2(j) = round(rand, 2); 
            
            cueVals1(j) = nums(j + numOfProperties * (numOfPairs - 1), 1);
            cueVals2(j) = nums(j + numOfProperties * (numOfPairs - 1), 2);
        end
        object1.Attributes = cueVals1;
        object2.Attributes = cueVals2;
        object1.BoxPos = rect1;
        object2.BoxPos = rect2;
        
        pairs(i).AttrNames = cueNames;
        pairs(i).Object1 = object1;
        pairs(i).Object2 = object2;
        pairs(i).BoxPos = rect;
        
        % ttb or delta
        if ~exist('deltaValue', 'var')
            pairs(i).CorrectAnswer = ttb(cueVals1, cueVals2); 
        else
            pairs(i).CorrectAnswer = delta(cueVals1, cueVals2, deltaValue); 
        end
    end  
end
