function decision = ttb(cue1, cue2)
    if cue2(1) > cue1(1)
        decision = 'B';
    else
        decision = 'A';
    end
end
