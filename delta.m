function choice = delta(cue1, cue2, deltaValue)
    decision = intmax('int64');
    for i=1:size(cue1, 2)
        if abs((cue1(i) - cue2(i)) / cue1(i)) < deltaValue
            continue;
        end
        decision = cue2(1) > cue1(1);
    end
    if decision == intmax('int64')
        decision = rand >= 0.5;
    end
    if decision == 1
        choice = 'B';
    else
        choice = 'A';
    end
end
