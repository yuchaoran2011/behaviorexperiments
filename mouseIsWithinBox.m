function [objectNum, cueNum] = mouseIsWithinBox(mouseX, mouseY, rect, numOfProperties)
    for boxNum = 1:size(rect, 2)
        if (mouseX >= rect(1, boxNum) && mouseX <= rect(3, boxNum) && ...
            mouseY >= rect(2, boxNum) && mouseY <= rect(4, boxNum))
            objectNum = ceil(boxNum/numOfProperties);
            cueNum = mod(boxNum, numOfProperties);
            if (cueNum == 0)
                cueNum = numOfProperties;
            end
            return
        end
    end
    objectNum = intmax('int64');
    cueNum = intmax('int64');
end
