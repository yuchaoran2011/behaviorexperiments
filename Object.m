classdef Object < handle
   properties
      Name
      Attributes
      AttrNumClicks
      AttrDuration
      BoxPos
   end
   methods
       % Object constructor
       function obj = Object(name, numOfProperties)
           obj.Name = name;
           obj.AttrNumClicks = zeros(numOfProperties, 1);
           obj.AttrDuration = zeros(numOfProperties, 1);
       end
       
       function updateNumClicks(obj, propertyNum)
           obj.AttrNumClicks(propertyNum) = obj.AttrNumClicks(propertyNum) + 1;
       end
       
       function updateDuration(obj, propertyNum, increment)
           obj.AttrDuration(propertyNum) = obj.AttrDuration(propertyNum) + increment;
       end
       
       function drawObject(object, window, titlePos)
           Screen('DrawText', window, object.Name, titlePos(1), titlePos(2), [0,0,0]);
           Screen('FillRect', window, [0, 0, 0], object.BoxPos); 
       end
       
       function drawTitle(object, window, titlePos)
           Screen('DrawText', window, object.Name, titlePos(1), titlePos(2), [0,0,0]);
       end
       
       function drawBoxes(object, window)
           Screen('FillRect', window, [0, 0, 0], object.BoxPos); 
       end
       
       function drawUponSelect(object, window, selectedBoxNum, posX, posY)
           % Draw non-selected boxes
           if (selectedBoxNum - 1 >= 1)
              Screen('FillRect', window, [0, 0, 0], object.BoxPos(:, 1:selectedBoxNum-1));
           end
           numOfBoxes = size(object.Attributes, 1);
           if (selectedBoxNum + 1 <= numOfBoxes)
              Screen('FillRect', window, [0, 0, 0], object.BoxPos(:, selectedBoxNum+1:numOfBoxes));
           end
           
           % Reveal text under selected box
           cue = num2str(object.Attributes(selectedBoxNum));
           Screen('DrawText', window, cue, posX, posY, [0,0,0]);
       end
   end
end
