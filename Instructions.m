classdef Instructions < handle
    properties
        Text
    end
    
    methods
        function readInstructionsFromFile(object, fileName)
            object.Text = fileread(fileName);
        end
        
        function drawText(object, window)
            [nx, ny, ~] = DrawFormattedText(window, object.Text, 'center', 'center', 0);
    
            % Show computed text bounding box, where bbox is the third
            % element in returned result above
            % Screen('FrameRect', window, 0, bbox);
            Screen('DrawText', window, 'Hit any key to continue.', nx+450, ny+50, [255, 0, 0, 255]);
        end
    end
end
